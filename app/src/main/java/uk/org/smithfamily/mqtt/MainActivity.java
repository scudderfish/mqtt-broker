package uk.org.smithfamily.mqtt;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.apache.log4j.BasicConfigurator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import io.moquette.BrokerConstants;
import io.moquette.interception.AbstractInterceptHandler;
import io.moquette.interception.messages.InterceptPublishMessage;
import uk.org.smithfamily.mqtt.broker.MQTTService;
import uk.org.smithfamily.mqtt.broker.ServerInstance;
import uk.org.smithfamily.mqtt.broker.util.InputFilterMinMax;
import uk.org.smithfamily.mqtt.broker.util.Utils;

public class MainActivity extends AppCompatActivity {

    public static final String LOGTAG = "MAIN";
    private MQTTService mService;
    private boolean mBound = false;
    Context context;
    private long received = 0;
    private MessageInterceptor interceptor = new MessageInterceptor(this);

    private ServiceConnection mConnection = new MQTTServiceConnection();


    EditText port, host;
    Switch serverControl;
    File confFile, passwordFile;
    TextView msgCount;

    @Override
    protected void onStart() {
        super.onStart();
        final Intent service = new Intent(this, MQTTService.class);
        this.bindService(service, mConnection, BIND_IMPORTANT);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_main);
        BasicConfigurator.configure();
        host = findViewById(R.id.host);
        port = findViewById(R.id.port);
        port.setFilters(new InputFilter[]{new InputFilterMinMax(1, 65535)});

        serverControl = findViewById(R.id.serviceRunning);
        serverControl.setEnabled(true);
        serverControl.setChecked(false);

        serverControl.setOnCheckedChangeListener((view,isChecked) ->{
                if (isChecked) {
                    startService();
                } else {
                    stopService(view);
                }

        });
        msgCount = findViewById(R.id.msgCount);

        confFile = new File(getApplicationContext().getDir("media", 0).getAbsolutePath() + Utils.BROKER_CONFIG_FILE);
        passwordFile = new File(getApplicationContext().getDir("media", 0).getAbsolutePath() + Utils.PASSWORD_FILE);
        Log.i(LOGTAG, confFile.getAbsolutePath());
        loadConfig();
        startService();
    }

    private Properties defaultConfig() {
        Properties props = new Properties();
        final File externalFilesDir = context.getExternalFilesDir(null);
        assert externalFilesDir != null;
        props.setProperty(BrokerConstants.PERSISTENT_STORE_PROPERTY_NAME, externalFilesDir.getAbsolutePath() + File.separator + BrokerConstants.DEFAULT_MOQUETTE_STORE_MAP_DB_FILENAME);
        props.setProperty(BrokerConstants.PORT_PROPERTY_NAME, "1883");
        props.setProperty(BrokerConstants.NEED_CLIENT_AUTH, "false");
        props.setProperty(BrokerConstants.HOST_PROPERTY_NAME, Utils.getBrokerURL(this));
        props.setProperty(BrokerConstants.WEB_SOCKET_PORT_PROPERTY_NAME, String.valueOf(BrokerConstants.WEBSOCKET_PORT));
        return props;
    }
    private void loadConfig() {

        try (InputStream input = new FileInputStream(confFile)) {
            Properties props = new Properties();
            props.load(input);
            updateUI(props);
            return;
        } catch (FileNotFoundException e) {
            Log.e("MAIN", "Config file not found. Using default config");
        } catch (IOException ex) {
            Log.e("MAIN", "IOException. Using default config");
        }
        Properties props = defaultConfig();
        updateUI(props);
    }

    private void updateUI(Properties props) {
        port.setText(props.getProperty(BrokerConstants.PORT_PROPERTY_NAME));
        host.setText(props.getProperty(BrokerConstants.HOST_PROPERTY_NAME));
        props.setProperty(BrokerConstants.WEB_SOCKET_PORT_PROPERTY_NAME, String.valueOf(BrokerConstants.WEBSOCKET_PORT));

    }

    public void startService() {
        if (mBound && mService != null) {
            Log.i(LOGTAG, "Service already running");
            return;
        }
        Intent serviceIntent = new Intent(this, MQTTService.class);

        Bundle bundle = new Bundle();
        bundle.putSerializable("config", defaultConfig());
        serviceIntent.putExtras(bundle);

        startService(serviceIntent);
        this.bindService(new Intent(this, MQTTService.class), mConnection, BIND_IMPORTANT);
    }

    public void stopService(View v) {
        Intent serviceIntent = new Intent(this, MQTTService.class);
        stopService(serviceIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            this.unbindService(mConnection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class MQTTServiceConnection implements ServiceConnection {

        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = ((MQTTService.LocalBinder) service).getService();
            mBound = ((MQTTService.LocalBinder) service).getServerStatus();
            final String ipaddr = Utils.getBrokerURL(context);
            Log.i(LOGTAG,String.format("Local IP: %s", ipaddr));
            host.setHint(ipaddr);
            serverControl.setChecked(true);
            ServerInstance.getServerInstance().addInterceptHandler(interceptor);
        }

        public void onServiceDisconnected(ComponentName arg0) {
            ServerInstance.getServerInstance().removeInterceptHandler(interceptor);
            mBound = false;
            mService = null;
            serverControl.setChecked(false);
        }

        @Override
        public void onBindingDied(ComponentName name) {
            System.out.println(name);
        }

        @Override
        public void onNullBinding(ComponentName name) {
            System.out.println(name);

        }
    }
    private class MessageInterceptor extends AbstractInterceptHandler {

        private final MainActivity activity;

        public MessageInterceptor(MainActivity mainActivity) {
            this.activity = mainActivity;
        }

        @Override
        public String getID() {
            return "MQTTMessageInterceptor";
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onPublish(InterceptPublishMessage msg) {
            received++;

            activity.runOnUiThread(() -> msgCount.setText(Long.toString(received)));
            super.onPublish(msg);
        }
    }
}
