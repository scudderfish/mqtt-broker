package uk.org.smithfamily.mqtt.broker;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.FutureTask;

import uk.org.smithfamily.mqtt.MainActivity;
import uk.org.smithfamily.mqtt.R;

import static uk.org.smithfamily.mqtt.broker.MQTTNotificationBroker.CHANNEL_ID;

public class MQTTService extends Service {

    private static final String TAG = "MQTTService";
    public static final String SERVICE_STARTED = "MQTT_SERVICE_STARTED";
    public static final String SERVICE_STOPPED = "MQTT_SERVICE_STOPPED";

    private Thread thread;
    private MQTTBroker broker;
    private boolean status = false;

    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public MQTTService getService() {
            return MQTTService.this;
        }

        public boolean getServerStatus() {
            return status;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Starting Notification Service");

        if(status) {
            sendStartedBroadcast();
            return START_NOT_STICKY;
        }
        //noinspection unchecked
        Map<String, String> map = (Map<String, String>) intent.getSerializableExtra("config");
        Properties config = new Properties();
        config.putAll(map);
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        try {
            broker = new MQTTBroker(config);
            FutureTask<Boolean> futureTask = new FutureTask<>(broker);
            if (thread == null || !thread.isAlive()) {
                thread = new Thread(futureTask);
                thread.setName("MQTT Server");
                thread.start();
                if (futureTask.get()) {
                    status = true;
                    Log.i("MQTT Service", "Thread is running");
                    sendStartedBroadcast();
                    Toast.makeText(this, "MQTT Broker Service started", Toast.LENGTH_LONG).show();
                }
            }

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("MQTT Broker")
                    .setContentText("Started MQTT Broker Service")
                    .setSmallIcon(R.drawable.ic_device_hub_black_24dp)
                    .setContentIntent(pendingIntent)
                    .build();

            startForeground(1, notification);
            return START_NOT_STICKY;
        } catch (Exception e) {
            Log.e(TAG, "Fweep!", e);
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
        status = false;
        this.stopSelf();
        return START_NOT_STICKY;
    }

    private void sendStartedBroadcast() {
        Intent broadCastIntent = new Intent();
        broadCastIntent.setAction(SERVICE_STARTED);

        sendBroadcast(broadCastIntent);
    }
    private void sendStoppedBroadcast() {
        Intent broadCastIntent = new Intent();
        broadCastIntent.setAction(SERVICE_STOPPED);

        sendBroadcast(broadCastIntent);
    }

    @Override
    public void onDestroy() {
        if (status) {
            try {
                Log.d(TAG, "Trying to stop mqtt server");
                broker.stopServer();
                thread.interrupt();
                status = false;
                sendStoppedBroadcast();
                Toast.makeText(this, "MQTT Broker Service stopped", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.d(TAG, "Server is not running");
        }
        super.onDestroy();
    }


}
