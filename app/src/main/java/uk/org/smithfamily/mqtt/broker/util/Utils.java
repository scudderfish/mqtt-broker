package uk.org.smithfamily.mqtt.broker.util;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

public class Utils {
    public static final String PASSWORD_FILE = "pwd.conf";
    public static final String BROKER_CONFIG_FILE = "mqtt.properties";

    public static String  getVersion() {
        String javaVersion = System.getProperty("java.version");
        System.out.format("Java Version = '%s'", javaVersion);
        return javaVersion;
    }

    @SuppressWarnings("deprecation")
    public static String getBrokerURL(Context ctx) {
        final WifiManager systemService = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        if (systemService != null) {
            final int ipAddress = systemService.getConnectionInfo().getIpAddress();
            return Formatter.formatIpAddress(ipAddress);
        } else {
            return "Unknown";
        }
    }
}

